package me.jboy.bubble;

import java.util.HashMap;
import java.util.Set;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class BubbleTroubleArena extends Arena {
	public static HashMap<String, Player> IG = new HashMap<String, Player>();

	public BubbleTroubleArena() {
	}

	public int id;

	public ArenaPlayer getAP(Player player) {
		ArenaPlayer ap = BattleArena.toArenaPlayer(player);
		return ap;
	}

	boolean running = false;

	@SuppressWarnings("deprecation")
	@Override
	public void onStart() {
		int countdown = 20;

		// HASH for players
		Set<ArenaPlayer> set = match.getPlayers();

		// MAIN Note: IG: In Game, hash to check if timers should run due to
		// issue.
		// Why do i put notes here no one else reads my code?
		for (ArenaPlayer arenaPlayer : set) {
			arenaPlayer.getPlayer().setLevel(countdown);
			arenaPlayer.getPlayer().getInventory()
					.setHelmet(new ItemStack(Material.GLASS));
			IG.put(arenaPlayer.getPlayer().getName(), arenaPlayer.getPlayer());
		}

		// task1

		if (running == false) {
			running = true;
			BukkitRunnable task1 = new BukkitRunnable() {
				@Override
				public void run() {

					for (ArenaPlayer arenaPlayer : set) {
						if (IG.containsValue(arenaPlayer.getPlayer().getName())
								|| IG.containsKey(arenaPlayer.getPlayer()
										.getName())) {
							arenaPlayer.getPlayer().setLevel(
									arenaPlayer.getPlayer().getLevel() - 1);

							Player p = (Player) arenaPlayer.getPlayer();

							// SET
							// TEST: @JTESTB102 p.setLevel(p.getLevel() - 1);
							// MESSAGES

							if (p.getPlayer().getLevel() < 5) {
								// TODO MAKE CHAT UTIL CLASS FOR MNGMENT OF
								// PREFIXES
								p.sendMessage("You need for air!");
							}
							if (p.getPlayer().getLevel() < 1) {
								// TODO MAKE CHAT UTIL CLASS FOR MNGMENT OF
								// PREFIXES
								p.sendMessage("You died");
								Bukkit.dispatchCommand(
										Bukkit.getConsoleSender(),
										"kill " + p.getName());
								IG.remove(p.getName(), p.getPlayer());
							}

						}
					}
				}
			};

			task1.runTaskTimer(BubbleTrouble.getPlugin(), 0L, 20L);
		} else {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.isOp()) {
					player.sendMessage("Op Only Message: Debug Mode: Task aleady running. Useing running instance (task)");
				}
			}
		}
	}
	// @Override
	// public void onFinish() {
	// }

}