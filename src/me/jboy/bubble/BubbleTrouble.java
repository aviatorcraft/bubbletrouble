package me.jboy.bubble;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.util.Log;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class BubbleTrouble extends JavaPlugin {

	public ArenaPlayer getAP(Player player) {
		ArenaPlayer ap = BattleArena.toArenaPlayer(player);
		return ap;
	}

	private static Plugin plugin;

	@SuppressWarnings({ "static-access" })
	public void onEnable() {
		this.plugin = this;
		BattleArena.registerCompetition(this, "BubbleTrouble", "bt",
				BubbleTroubleArena.class);
		saveDefaultConfig();
		Log.info("[" + getName() + "] v" + getDescription().getVersion()
				+ " enabled!");

	}

	@Override
	public void reloadConfig() {
		super.reloadConfig();
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	public void onDisable() {
		plugin = null;// To stop memory leeks

	}

}
